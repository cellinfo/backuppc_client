#!/bin/bash

# Automatisation d'installation et configuration de BackupPC pour Windows 10 {{{
# via Bash on Ubuntu
# Institut de Physique de Rennes UMR6251
# Jérémy Gardais − Mail 2019
# }}}
# How-to use {{{
# With PowerShell
## This script can be launch with PowerShell :
## bash -c "./install_backuppc_windows_bash_on_linux.sh"
# }}}

# Variable {{{
## Valeur de fin
SUCCESS=0
ERROR=1

backuppc_user="backuppc"
backuppc_homedir="/var/lib/backuppc"
backuppc_sshkey="from=\"129.20.203.16\" ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDIhMc8ixQXfWDACJy4q0v8T877UxahhCjO51PQFzylwVpf88LX3yWeDrWIW0NRu0zoSm396mig918OpD5ggqML/QbYbQsoDdAFUV/tK4JU6UJgEQIl25MOcUBCFepsFBGS09CH/V07xSUqSP/+beeTRLNO2CQzk3S2y3YfkXpM7KmOGfeLgoCaQAcxIkgLXeM3TpCZEzJDlZ8c8k/DjVvsgwCpQktYzNo2b37KHLLfgyW9KSo6N9sReUuNQjS6lu8rjrXfc6+J0pY2D6IxWptTWL/JVrhFCUqe4QQy+xYjoR41wqnAQyl/kOcyBNhSvojMKwQT6vlPwru6pOno16/X backuppc@backuppc.ipr.univ-rennes1.fr"

user_main_backup_dir=""

## Couleur {{{
BLACK='\033[49;30m'
BLACKB='\033[49;90m'
RED='\033[0;31m'
REDB='\033[1;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[94;49m'
MAGENTA='\033[0;35m'
CYAN='\033[36;49m'
WHITE='\033[0;37m'
BOLD='\033[1m'
RESET='\033[0m'
## }}}
# }}}
# Fonctions {{{
function getMainBackupDir()  ## {{{
{
	local input_dir
	local return_value

	printf '%b' "${GREEN}Merci de saisir ici (copier/coller avec le clic droit), le chemin du dossier à sauvegarder :${RESET} "
	read -r input_dir

	### Transform Windows path to Unix path
	user_main_backup_dir=$(wslpath "${input_dir}")

	### Verify the directory exist
	if [ ! -d "${user_main_backup_dir}" ]; then
		printf '%b' "${BOLD}${input_dir}${RESET} ne semble ${BOLD}pas${RESET} être un dossier. Merci de vérifier votre saisie.${RESET}\\n"
		return_value="${ERROR}"
	else
		#printf '%b' "${GREEN}Le dossier à sauvegarder est : ${input_dir}.${RESET}\\n"
		return_value="${SUCCESS}"
	fi

	return "${return_value}"
}
## }}}

function addUserForBackuppc()  ## {{{
{
	local userLogin="${1}"    ### eg. 'backuppc'
	local userHomedir="${2}"  ### eg. '/var/lib/backuppc'
	if ! getent passwd "${userLogin}" > /dev/null 2>&1; then
		printf '%b' "${BOLD}L'utilisateur ${userLogin} n'existe pas, tentative de création.${RESET}\\n"
		useradd --system --shell /bin/sh --home-dir "${userHomedir}/" --create-home -- "${userLogin}"
	fi
	chown -R -- "${userLogin}:" "${userHomedir}"
}
## }}}
function manageSshKeyForBackuppc()  ## {{{
{
	local userLogin="${1}"             ### eg. 'backuppc'
	local backuppcSshKey="${2}"        ### eg. 'AAAAA...

	local userHomedir
	userHomedir="$(getent passwd "${userLogin}" | cut -d: -f6)"

	### Create ssh directory for BackupPC's user
	mkdir -p -- "${userHomedir}/.ssh"
	chmod 0700 -- "${userHomedir}/.ssh"
	printf '%b' "${backuppcSshKey}" > "${userHomedir}/.ssh/authorized_keys"
	chmod 0600 -- "${userHomedir}/.ssh/authorized_keys"
	chown -R -- "${userLogin}:" "${userHomedir}"
}
## }}}

function ensureSshdIsInstalled()  ## {{{
{
	### `command -v sshd` still return old value even after openssh-server was removed
	### So we can't be sure of openssh-server state.
	if ! hash sshd 2>/dev/null; then
		printf '%b' "${BOLD}openssh-server non installé, tentative d'installation.${RESET}\\n"

		if   [ $(command -v aptitude) ];  then aptitude install -y --quiet=5 -- openssh-server > /dev/null
		elif [ $(command -v apt) ];       then apt install -y openssh-server > /dev/null
		elif [ $(command -v yum) ];       then yum install -y openssh-server
		elif [ $(command -v zypper) ];    then zypper install -y openssh-server
		else
			printf '%b' "${REDB}Merci d’installer openssh-server sur votre machine, installation annulée.${RESET}\\n"
			return "$ERROR"
		fi
	fi
}
## }}}
function ensureSshdIsRunning()  ## {{{
{
	### Ensure to completely restart sshd
	service ssh --full-restart > /dev/null
}
## }}}

# }}}

# Récupération des informations de l'utilisateur {{{
getMainBackupDir
if [ "$?" == "${ERROR}" ]; then
	printf '%b' "${REDB}La récupération du dossier principal à sauvegarder a échouée, installation annulée.${RESET}\\n"
	exit "${ERROR}"
fi
# }}}
# Gestion des paquets {{{
## Mettre à jour les dépôts
apt update -- > /dev/null 2>&1

## Installer aptitude
### aptitude permet une meilleure résolution des dépendances/erreurs/... que apt
### apt-get est déprécié au profit de apt ou aptitude
apt install -y -- aptitude > /dev/null 2>&1

## Mettre à jour le système
aptitude full-upgrade -y --quiet=5 > /dev/null
# }}}
# Gestion de l'utilisateur pour BackupPC {{{
addUserForBackuppc "${backuppc_user}" "${backuppc_homedir}"
if [ "$?" != "${SUCCESS}" ]; then
	printf '%b' "${REDB}La création de l'utilisateur ${backuppc_user} a échouée, installation annulée.${RESET}\\n"
	exit "${ERROR}"
fi
manageSshKeyForBackuppc "${backuppc_user}" "${backuppc_sshkey}"

# }}}
# Gestion de SSH {{{
ensureSshdIsInstalled
if [ "$?" != "${SUCCESS}" ]; then
	printf '%b' "${REDB}L'installation du serveur ssh a échoué, installation annulée.${RESET}\\n"
	exit "${ERROR}"
fi
ensureSshdIsRunning
if [ "$?" != "${SUCCESS}" ]; then
	printf '%b' "${REDB}Le serveur ssh n'a pas correctement démarré, installation annulée.${RESET}\\n"
	exit "${ERROR}"
fi

# }}}
exit "${SUCCESS}"
