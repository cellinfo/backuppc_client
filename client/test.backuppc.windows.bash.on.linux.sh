#!/bin/sh

# Ce script peut être lancé depuis un PowerShell en administrateur avec la commande:
# bash -c "./install.backuppc.bash.on.linux.sh"

# Gestion des paquets

## Mettre à jour les dépôts
apt update

## Installer aptitude
apt install -y aptitude

## Mettre à jour le système (sans interaction)
DEBIAN_FRONTEND=noninteractive aptitude -y -o Dpkg::Options::=--force-confdef -o Dpkg::Options::=--force-confold full-upgrade

# Gestion de SSH

## Installation du serveur SSHD
### Nettoyer les éventuelles précédentes installations
DEBIAN_FRONTEND=noninteractive aptitude remove -y openssh-server

DEBIAN_FRONTEND=noninteractive aptitude purge -y openssh-server
DEBIAN_FRONTEND=noninteractive aptitude install -y openssh-server

## S'assurer de démarrer le service
### Systemd ne semble pas disponible par défaut
service ssh start