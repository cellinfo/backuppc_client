:: Ouvrir une fenetre de demande de dossier
@echo off
setlocal
set "psCommand="(new-object -COM 'Shell.Application')^
.BrowseForFolder(0,'Selectionnez votre dossier a sauvegarder.',0,0).self.path""
for /f "usebackq delims=" %%I in (`powershell %psCommand%`) do set "input_dir_win=%%I"
setlocal enabledelayedexpansion
@echo !input_dir_win!
endlocal

:: Mettre input_dir au format unix
for /f "delims=" %%i in ('c:\backuppc_ipr\bin\cygpath.exe --unix "%input_dir_win%"') do Set input_dir=%%i

:: Demande l'adresse mail dans le terminal
set /p input_mail=Saisissez votre adresse email : 
@echo %input_mail%

:: Recuperer la clef ssh de l'hote
set hostkeyfile=C:\backuppc_ipr\etc\ssh_host_ecdsa_key.pub
set /p hostkey=<%hostkeyfile%

:: Mettre le nom de la machine dans une variable
for /f "delims=" %%i in ('hostname') do Set host=%%i
set domain=spm.univ-rennes1.fr

:: Recuperer l'adresse ip de la machine
for /F "tokens=4 delims= " %%i in ('route print ^| find " 0.0.0.0"') do set localIp=%%i
@echo Your IP Address is: %localIp%

:: Definir le chemin du fichier pl
::set filepl=%UserProfile%\Desktop\%host%.%domain%.pl
set filepl=C:\%host%.%domain%.pl

:: Definir la liste des exclusion
set exclude=['Owncloud','thumbs.db','$Recycle.bin', 'System Volum*','desktop.ini','Cache','RECYCLER','temp','Perso', '~$*']

:: Creation du fichier pl
echo $Conf{XferMethod} = 'rsync'; > %filepl%
echo $Conf{RsyncShareName} = ['%input_dir%']; >> %filepl%
echo $Conf{BackupFilesExclude} = {'*' =^> %exclude%}; >> %filepl%
echo # host:%host%.%domain% >> %filepl%
echo # hostkey:%hostkey% >> %filepl%
echo # mail:%input_mail% >> %filepl%
echo # ip:%localIp% >> %filepl%

:: Cacher l'utilisateur backuppc
reg add "HKEY_Local_Machine\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList" /t "REG_DWORD" /v "backuppc" /d "00000000" /f
