#!/usr/bin/bash
printf "Langue de votre Windows ? (en, es, fr) [fr]"
read lang
if [[ $lang == "" ]]; then lang="fr"; fi

case $lang in
    "en") group_name="Backup Operators" ;;
    "es") group_name="Operadores de copia de seguridad" ;;
    "fr") group_name="Opérateurs de Sauvegarde" ;;
    *)    group_name="Backup Operators" ;;
esac

ip_ur1=$(wget http://checkip.dyndns.org/ -O - -o /dev/null | cut -d" " -f 6 | cut -d\< -f 1 | cut -d'.' -f1-2)

if [[ ${ip_ur1} != "129.20" ]]; then
  printf "Merci de vous connecter au réseau 'univ-rennes1' et relancer le 01_install_backuppc.exe\n"
  read enderror
  exit 1
fi

net user backuppc $(openssl rand -base64 8) /add     # Creation compte backuppc (mot de passe aleatoire)
#net localgroup "Administrateurs" backuppc /add       # Ajout des droits admin a backuppc
net localgroup "$group_name" backuppc /add
mkpasswd -l > "/etc/passwd"
mkgroup -l > "/etc/group"
ssh-host-config -y -w $(openssl rand -base64 8)      # configuration de ssh
mkdir -p "/home/backuppc/.ssh"
echo "from=\"129.20.203.16\" ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDIhMc8ixQXfWDACJy4q0v8T877UxahhCjO51PQFzylwVpf88LX3yWeDrWIW0NRu0zoSm396mig918OpD5ggqML/QbYbQsoDdAFUV/tK4JU6UJgEQIl25MOcUBCFepsFBGS09CH/V07xSUqSP/+beeTRLNO2CQzk3S2y3YfkXpM7KmOGfeLgoCaQAcxIkgLXeM3TpCZEzJDlZ8c8k/DjVvsgwCpQktYzNo2b37KHLLfgyW9KSo6N9sReUuNQjS6lu8rjrXfc6+J0pY2D6IxWptTWL/JVrhFCUqe4QQy+xYjoR41wqnAQyl/kOcyBNhSvojMKwQT6vlPwru6pOno16/X backuppc@backuppc.ipr.univ-rennes1.fr" > /home/backuppc/.ssh/authorized_keys # Cle ssh permettant l'authentification du serveur
chown -R backuppc "/home/backuppc"
echo "AllowUsers backuppc" >> /etc/sshd_config        # securisation ssh : n'autoriser que backuppc en ssh
echo "PasswordAuthentication no" >> /etc/sshd_config  # securisation ssh : refuser connexion ssh avec mot de passe
netsh advfirewall firewall add rule name="SSH-Hole Port 22" dir=in action=allow protocol=TCP localport=22 remoteip=129.20.203.16 profile=domain,private,public                  # ouverture port pour ssh
netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow remoteip=129.20.203.16 profile=domain,private,public   # ouverture port pour ping
net start sshd                                        # ssh au demarrage

if [[ ! -f /cygdrive/c/backuppc_ipr/etc/ssh_host_ecdsa_key.pub ]]; then
  printf "Erreur lors de l'installation, merci de relancer avec les droits Administrateur\n"
  read enderror
fi

printf "01_script_install_backuppc terminé avec succès, appuyez sur 'Entrée'\n"
read endpoint
exit 0

