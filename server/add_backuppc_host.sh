#!/bin/bash

# Script pour ajouter une machine à sauvegarder dans BackupPC
# Modifie les fichiers:
#	 /etc/hosts
#	 /etc/backuppc/hosts
#	 /etc/backuppc/NOM_UTILISATEUR.NOM_MACHINE.pl
#	 /var/lib/backuppc/.ssh/config
#	 /var/lib/backuppc/.ssh/known_hosts

#### HOW TO ####
# scp file.pl backuppc.ipr.univ-rennes1.fr:
# ssh backuppc.ipr.univ-rennes1.fr
# cd /etc/backuppc
# sudo ./add_backuppc_host.sh ~/pt-gilles.spm.univ-rennes1.fr.pl

#### DÉPENDANCES ####
# ldapsearch (pkg ldap-utils)
if [ ! $(command -v ldapsearch) ]; then
	aptitude install dos2unix ldap-utils
fi

# Liste des fichiers
system_hosts="/etc/hosts"
backuppc_config_dir="/etc/backuppc"
backuppc_host_file="${backuppc_config_dir}/hosts"
backuppc_home_dir="/var/lib/backuppc"
backuppc_known_host_file="${backuppc_home_dir}/.ssh/known_hosts"
backuppc_ssh_config_file="${backuppc_home_dir}/.ssh/config"

config_pl_path="${1}"
config_pl_name=$(basename -- "${config_pl_path}")

#### VÉRIFIER QUE L’ON A BIEN LES DROITS ADMIN ####
if [ "$EUID" -ne 0 ]; then
	printf '\e[1;31m%-6s\e[m' "À lancer avec les droits administrateur"
	exit
fi

#### VÉRIFIER QUE LE FICHIER EXISTE BIEN ####
if [ ! -f "${config_pl_path}" ]; then
	printf '\e[1;31m%-6s\e[m' "Installation annulée, le fichier n'existe pas !"
	exit
fi

#### VÉRIFIER QUE LE FICHIER EST BIEN EN UTF-8 ####
dos2unix ${config_pl_path}

# Liste des informations
ip=$(grep "# ip:" ${config_pl_path} | cut -d':' -f 2)
ip_ur1=$(echo ${ip} | cut -d'.' -f1-2)
temp_fqdn=$(host ${ip} | cut -d' ' -f 5)
# Remove the last character from $temp_fqdn: '.'
fqdn="${temp_fqdn%?}"
hostname=$(echo ${fqdn} | cut -d'.' -f1)
mail=$(grep "# mail:" ${config_pl_path} | cut -d':' -f 2)
mail_ur1=$(echo ${mail} | cut -d'@' -f2)
username=$(ldapsearch -ZZ -H ldap://ldap.univ-rennes1.fr -LLL '(mail='"${mail}"')' -b "dc=univ-rennes1,dc=fr" -x uid | grep "^uid" | cut -d" " -f2)
backup_username=$(grep "# username:" ${config_pl_path} | cut -d':' -f 2)

#printf "IP: ${ip}\n"
#printf "temp_fqdn ${temp_fqdn}\n"
#printf "fqdn ${fqdn}\n"
#printf "hostname ${hostname}\n"
#printf "mail: ${mail}\n"
#printf "username: ${username}\n"
#printf "backup_username: ${backup_username}\n"

#### TESTS ####
# Check IP
if [[ ${ip_ur1} != "129.20" ]]; then
	printf "La machine cliente n'était pas sur le réseau UR1\n"
	exit 1
fi

# Check mail
if [[ ${mail_ur1} != "univ-rennes1.fr" ]]; then
	printf "Le client n'a pas renseigné une adresse UR1\n"
	exit 1
fi

#### AJOUTER L HÔTE DANS ${BACKUPPC_CONFIG_DIR}/HOSTS ####
# jfade.pr079076.spm.univ-rennes1.fr	0	jfade
if [[ ! $(grep "${username}.${hostname}" "${backuppc_host_file}") ]]; then
	echo "${username}.${hostname}	0	${username}" >> "${backuppc_host_file}"
	#echo "WRITE TO ${backuppc_host_file}: ${username}.${hostname}	0	${username}"
else
	printf '\e[1;31m%-6s\e[m' "Installation annulée, la ligne ${username}.${hostname} existe déjà dans ${backuppc_host_file} !"
	exit
fi

#### AJOUTER LA CLEF SSH DE LA MACHINE DE L'UTILISATEUR ####
# pr079076.spm.univ-rennes1.fr ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBIFkIxzrMTZ/m9AgA7Jc+XnKdayDwUtehGOPo5m4i9yK5mCMM/iOrTOPxubey3YcQBuuqHNNRWbDV6n0z5KGvBU=
if [[ $(grep "# hostkeycontent:" "${config_pl_path}") ]]; then
	hostkey=$(grep "# hostkeycontent:" ${config_pl_path} | cut -d':' -f 2)
else
	hostkey=$(grep "# hostkey:" ${config_pl_path} | cut -d':' -f 2)
fi
if [[ ! $(grep "${fqdn}" "${backuppc_known_host_file}") ]]; then
	echo "${fqdn} ${hostkey}" >> "${backuppc_known_host_file}"
	#echo "WRITE TO ${backuppc_known_host_file}: ${fqdn} ${hostkey}"
else
	printf '\e[1;31m%-6s\e[m' "Installation annulée, la clef ssh pour ${username}.${fqdn} existe déjà dans ${backuppc_known_host_file} !"
	#exit
	#### NO EXIT !!! #### NEED ${fqdn} and NOT ${username}.${fqdn}
fi

#### DÉFINIR L'HÔTE DANS LA CONFIGURATION SSH ####
# Host jfade.pr079076
#	 hostname pr079076.spm.univ-rennes1.fr
if [[ ! $(grep "${username}.${hostname}" "${backuppc_ssh_config_file}") ]]; then
	cat << EOF >> "${backuppc_ssh_config_file}"
Host ${username}.${hostname}
	hostname ${fqdn}
EOF

	# Pour une machine Windows DSI, backup_user particulier
	if [[ ${backup_username} ]]; then
	cat << EOF >> "${backuppc_ssh_config_file}"
	user "${backup_username}"

EOF
	else
	cat << EOF >> "${backuppc_ssh_config_file}"

EOF
	fi

fi

#### DÉFINIR LE NOM D'HOTE POUR LE SYSTEME ####
if [[ ! $(grep "${ip} ${username}.${hostname}" "${system_hosts}") ]]; then
	echo "${ip} ${username}.${hostname}" >> "${system_hosts}"
	#echo "WRITE TO ${system_hosts}: ${ip} ${username}.${hostname}"
else
	printf '\e[1;31m%-6s\e[m' "Le nom d'hôte ${username}.${hostname} est déjà connu du système (${system_hosts})!"
fi

#### DÉPLACER LE FICHIER DANS LE RÉPERTOIRE DE BACKUPPC ####
if [ ! -f "${backuppc_config_dir}/${username}.hostname" ]; then
	mv "${config_pl_path}" "${backuppc_config_dir}/${username}.${hostname}.pl"
	chown backuppc:www-data "${backuppc_config_dir}/${username}.${hostname}.pl"
	#printf "TODO||TOREMOVE: mv ${config_pl_path}" "${backuppc_config_dir}/${username}.${hostname}.pl"
else
	printf '\e[1;31m%-6s\e[m' "Installation annulée, le fichier ${username}.${hostname}.pl existe déjà dans ${backuppc_config_dir}/ !"
	exit
fi

#### recharger la configuration de backuppc ####
service backuppc reload

