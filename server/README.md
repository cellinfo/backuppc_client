# BackupPC Server

1. […](#…)
2. [Erreur type ajout de poste](#erreur-type-ajout-de-poste)
    * [hostkey vide (fichier .pl)](#hostkey-vide-fichier-pl)
    * [Mauvais réseau](#mauvais-réseau)
    * [Clé inaccessible](#clé-inaccessible)
    * [Erreur Cygwin cyggcc](#erreur-cygwin-cyggcc)
3. [Erreur type pour une sauvegarde](#erreur-type-pour-une-sauvegarde)
    * [Ping too slow](#ping-too-slow)
    * [Unable to read 4 bytes](#unable-to-read-4-bytes)
    * [Début de sauvegarde très long](#debut-de-sauvegarde-très-long)
    * [Erreur de transfert](#erreur-de-transfert)
3. [Mail type](#mail-type)
    * [Sauvegarde en cours](#sauvegarde-en-cours)
    * [Sauvegarde prévue](#sauvegarde-prévue)

## Erreur type ajout de poste

### hostkey vide-fichier .pl
Si le fichier **.pl** fourni par l'utilisateur n'a pas de paramètre **hostkey**, cela signifie que SSH s'est mal installé.
  * **Windows**: relancer les scripts ``c:\backuppc_ipr\01_install_backuppc.exe`` puis ``c:\backuppc_ipr\02_fichier_config.bat`` comme indiqué dans la [documentation][documentation backuppc windows].
  * **Linux**: relancer le script ``/tmp/install_auto.sh``
  * **Mac**: relancer le script ``/tmp/install_auto.sh``

```
Bonjour,

Il semble il y avoir eu un problème lors de l'activation d'un service sur ton poste nécessaire à la sauvegarde. Est-ce que tu peux…

Puis me renvoyer le fichier **.pl** généré. Si il n'est pas différent du précédent je passerai te voir afin de déterminer le problème :)
```

### Mauvais réseau
Si l'utilisateur n'était pas sur le réseau de l'Université lors de l'installation (adresse IP différente de 129.20.xxx.xxx).
  * **Windows**: relancer le script ``c:\backuppc_ipr\02_fichier_config.bat`` comme indiqué dans la [documentation][documentation backuppc windows].
  * **Linux**: relancer le script ``/tmp/install_auto.sh``
  * **Mac**: relancer le script ``/tmp/install_auto.sh``

```
Bonjour,

Il semblerait que tu n'étais pas connecté sur le réseau de l'Université lors de l'installation de l'outil de sauvegarde. Est-ce que tu peux…

Puis m'envoyer le nouveau fichier **.pl** généré. Si le problème persiste je passerai te voir pour qu'on détermine d'où ça vient :)
```

### Clé inaccessible
  * Si l'utilisateur est sur **MacOS**, il est possible que les clefs SSH de la machine n'est pas encore été créées. Pour résoudre ça :
    * Tenter une connexion SSH vers la machine de l'utilisateur.
    * Demander à l'utilisateur de relancer le script :

```
Bonjour,

Il semble que tu utilises un système MacOS qui n'avait pas créée tous les fichiers nécessaires au bon fonctionnement de la sauvegarde.
Je viens de tenter une simple connexion vers ton ordinateur, ce qui peut avoir résolu le problème, est-ce que tu peux retester la procédure comme indiquée dans la documentation ?

Merci.
```

### Erreur Cygwin cyggcc
  * Symptôme pour un poste Windows, étant donné que c'est lié à Cygwin :
    * La sauvegarde d'un poste client ne fonctionne plus et indique **Unable to read 4 bytes**.
    * Le poste n'éteint pas, ou très peu éteint, il s'agissait principalement de mise en veille prolongée.
    * Lors de la connexion SSH sur le poste client, SSH indique que la connexion a été réinitialisée.
    * Lors de la connexion SSH sur le poste client, lorsque le shell s'ouvre on a le droit à ~4 belles lignes d'avertissement :

```
0 [main] perl 10672 child_info_fork::abort: C:\cygwin\bin\cyggcc_s-1.dll: Loaded to different address: parent(0x320000) != child(0x3A0000)
```

  * Solution : Un simple redémarrage "complet" de l'ordinateur a suffit. Impossible de déterminer, pour le moment, si c'est lié au fait que l'ordinateur soit rarement éteint ou une éventuelle mauvaise manipulation dans les processus.

## Erreur type pour une sauvegarde

### Ping too slow
Erreur qui peut apparaître lors de la communication entre le serveur BackupPC et une machine cliente. Ça peut s'expliquer si il y a par exemple des sauvegardes en cours qui sollicitent déjà le système.

Vérifier l'état des sauvegardes :
https://backuppc.ipr.univ-rennes1.fr/index.cgi?action=summary


### Unable to read 4 bytes
Il s'agit la plupart du temps d'un problème de connexion SSH vers le poste client :

  * Est-ce que la règle de parefeu est bien présente pour l'IP 129.20.203.16 et le port 22 ?
    * Parefeu, règles avancées, règles entrantes,…

#### Windows DSI
* Si il s'agit d'un poste Windows 7, il suffit de suivre la procédure d'installation :

```
Bonjour,

Ton poste est un Windows installé par la DSI. Est-ce que tu as bien suivi la documentation spécifique :
https://ipr.univ-rennes1.fr/intranet-ipr/sauvegarde-des-donnees-sur-serveur-backuppc-ipr?mtop=intra#Postes_DSI

Bonne journée.
```

* Si il s'agit d'un poste Windows 10, le centre logiciel ne propose pas encore l'utilitaire BackupPC pour l'IPR. Il faut donc considérer le poste comme un [Windows IPR](#windows-ipr).

```
Bonjour,

Si ton poste est un Windows 10, il est seuleument en partie géré par la DSI, tu peux suivre la documentation de l'IPR :
https://ipr.univ-rennes1.fr/intranet-ipr/sauvegarde-des-donnees-sur-serveur-backuppc-ipr?mtop=intra#Installation

Bonne journée.
```
  * Il est plus nécessaire de lancer le `cygwin terminal` en administrateur puis de corriger les droits sur le répertoire de l'utilisateur `backuppc` :

``` sh
chown -R "NOM_DE_LA_MACHINE+backuppc" /home/backuppc
chgrp -R "Système" /home/backuppc
```

  * Vérifier les règles de parefeu (Parefeu, règles avancées, règles entrantes,…) et les ajouter le cas échéant :

``` sh
netsh advfirewall firewall add rule name="SSH-Hole Port 22" dir=in action=allow protocol=TCP localport=22 remoteip=129.20.203.16 profile=domain,private,public
netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow remoteip=129.20.203.16 profile=domain,private,public
```

  * Vérifier que le service SSHD de Cygwin est bien démarré, le cas échéant :

``` sh
net start sshd
```

#### Windows IPR
  * La machine est-elle bien allumée ?
  * La machine répond au ping depuis le serveur de sauvegarde ?
  * Est-ce que le le premier script ``c:\backuppc_ipr\01_install_backuppc.exe`` a bien été exécuté en tant qu'Administrateur ?
  * Est-ce que le le premier script ``c:\backuppc_ipr\01_install_backuppc.exe`` s'est bien terminé par le message "01_script_install_backuppc terminé avec succès" ?
  * S'assurer que le nom de l'utilisateur et le nom de l'ordinateur ne contiennent pas d'accent ou de caractères spéciaux.

#### Poste Linux
* Le serveur SSH est-il bien démarré ?

``` sh
ss -ltn | grep 22
```

  * Le démarrer et l'activer de façon automatique pour le prochain démarrage :

``` sh
systemctl enable sshd
systemctl start sshd
```

### Début de sauvegarde très long
Si une sauvegarde, complète ou incrémentielle, est lancée et que la liste des fichiers à sauvegarder (**/var/lib/backuppc/pc/${HOSTNAME}/NewFileList**) reste désespérement vide après plus de 30 minutes :
  * Vérifier que les répertoires à sauvegarder (**RsyncShareName**) sont cohérents entre l'interface web et le fichier de configuration (**/etc/backuppc/${HOSTNAME}.pl).
    * Une différence a déjà été remarquée après avoir renommé un poste client.
  * Vérifier que le fichier de configuration (**/etc/backuppc/"${HOSTNAME}".pl**) existe bien, porte le bon **nom** et la bonne **extension**.

Note : La récupération de la liste des nouveaux fichiers à sauvegarder prends à peine deux minutes pour plus de 40Gb de données.

### Erreur de transfert

Après une sauvegarde réussie, il est possible que certains fichiers n'aient pas pu être sauvegardés correctement. Ces fichiers sont comptés et listés comme "Erreur de transfert". Plusieurs raisons :
  * "Légitimes" :
    * Un verrou sur un fichier n'ait pas sauvegardé (exemple : Verrou sur la base Thunderbird, un fichier bureautique,…).
  * Plus problématique :
    * **failed: Permission denied (13)** - droits insuffisants sur les fichiers.

* Pour résoudre le problème de droits insuffisants sur les fichiers :
  * S'assure que l'utilisateur utilisé sur le poste client soit bien dans le groupe de l'utilisateur dont les données doivent être sauvegardées :

``` sh
ls -l /home/$USER

id backuppc

# Sous GNU/Linux :
sudo addgroup backuppc "${USER_MAIN_GRP}"
# Sous MacOSX
sudo dseditgroup -o edit -a backuppc -t user "${USER_MAIN_GRP}"
```

  * Vérifier que les fichiers appartiennent bien au groupe principal de l'utilisateur.
  * Vérifier que le groupe principal de l'utilisateur a bien les droits de lecture (et d'exécution pour les dossiers).
  * Il est possible de donner au groupe les mêmes droits que ceux de l'utilisateur :

``` sh
chmod -R g=u ~/.mozilla ~/.thunderbird ~/.icedove ~/.gimp-2.8 ~/.java ~/.config ~/.local
```

  * Dans tous les cas, penser à **éviter** la **clef SSH privée** de l'utilisateur !

## Mail type

### Sauvegarde en cours

```
Bonjour,

Ta configuration est maintenant en place. Une sauvegarde aurait due être lancée automatiquement par le serveur sous peu mais j’ai préféré tester directement en lançant une sauvegarde manuellement dès maintenant.

La sauvegarde est en cours et semble bien fonctionner. Ta machine rencontrera peut-être quelques ralentissements pendant une ou deux heures. Si tu éteints ton pc avant la fin de la sauvegarde, pas d’inquiétude elle sera reprise automatiquement.

Tu peux accéder aux données sauvegardées et modifier la configuration depuis l'interface web:
https://backuppc.ipr.univ-rennes1.fr/
```

### Sauvegarde prévue

```
Bonjour,

Ta configuration est maintenant en place. Une sauvegarde devrait avoir lieu la prochaine fois que ta machine sera présente sur le réseau.

Lors de la sauvegarde, ta machine peut rencontrer quelques ralentissements pendant une ou deux heures. Si tu éteints ton pc avant la fin de la sauvegarde, pas d’inquiétude elle sera reprise automatiquement.

Tu peux accéder aux données sauvegardées et modifier la configuration depuis l'interface web:
https://backuppc.ipr.univ-rennes1.fr/
```

[documentation backuppc windows]: https://ipr.univ-rennes1.fr/intranet-ipr/sauvegarde-des-donnees-sur-serveur-backuppc-ipr#Postes_Windows
[documentation backuppc windows desinstallation]: https://ipr.univ-rennes1.fr/intranet-ipr/sauvegarde-des-donnees-sur-serveur-backuppc-ipr#Désinstallation
